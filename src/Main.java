import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException {
        Set<String> words = getAllWords();

        long startTime = System.currentTimeMillis();

        Set<String> nineLetterWords = new HashSet<>();
        for (String word : words) {
            if (word.length() == 9) {
                nineLetterWords.add(word);
            }
        }

        Set<String> finalWords = new HashSet<>();
        for (String word : nineLetterWords) {
            if (canReduceToSingleLetter(word, words)) {
                finalWords.add(word);
            }
        }

        System.out.println("Words count: " + finalWords.size());

        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Execution time: " + duration + " millis");
    }

    private static Set<String> getAllWords() throws IOException {
        URI uri = URI.create("https://raw.githubusercontent.com/nikiiv/JavaCodingTestOne/master/scrabble-words.txt");
        URL url = uri.toURL();
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

        Set<String> words = new HashSet<>();
        String line;
        while ((line = reader.readLine()) != null) {
            words.add(line.trim());
        }
        reader.close();
        return words;
    }

    private static boolean canReduceToSingleLetter(String word, Set<String> words) {
        if (word.equals("A") || word.equals("I")) {
            return true;
        }
        if (!words.contains(word)) {
            return false;
        }
        for (int i = 0; i < word.length(); i++) {
            StringBuilder reducedWord = new StringBuilder(word);
            reducedWord.deleteCharAt(i);
            if (canReduceToSingleLetter(reducedWord.toString(), words)) {
                return true;
            }
        }
        return false;
    }
}
